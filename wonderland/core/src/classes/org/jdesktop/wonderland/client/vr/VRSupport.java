/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jdesktop.wonderland.client.vr;

import com.jme.image.Texture;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.CameraNode;
import com.jme.scene.Node;
import com.jogamp.opengl.util.texture.TextureIO;
import com.oculusvr.capi.OvrLibrary;
import com.oculusvr.capi.OvrQuaternionf;
import com.oculusvr.capi.OvrSizei;
import com.oculusvr.capi.OvrVector3f;
import com.sun.jna.Pointer;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.time.Clock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLContext;
import javax.media.opengl.GLException;
import static javax.media.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static javax.media.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;
import org.jdesktop.mtgame.CameraComponent;
import org.jdesktop.mtgame.RenderBuffer;
import org.jdesktop.mtgame.RenderUpdater;
import org.jdesktop.mtgame.TextureRenderBuffer;
import org.jdesktop.mtgame.WorldManager;
import org.jdesktop.mtgame.processor.WorkProcessor;
import org.jdesktop.wonderland.client.cell.view.ViewCell;
import org.jdesktop.wonderland.client.jme.SceneWorker;
import org.jdesktop.wonderland.client.jme.ViewManager;
import org.jdesktop.wonderland.client.jme.ViewProperties;

/**
 *
 * @author cramirez
 */
public class VRSupport extends OculusSupport implements RenderUpdater {

    final static VRSupport vrSupport = new VRSupport();

    private static final TextureRenderBuffer[] eyeRenderBuffer = new VRTextureRenderBuffer[EYE_COUNT];
    public static final float CAMERA_ELEVATION = 2.2f;//2.2f;

    /* Avoid excesive garbage */
    private static final Quaternion orientation[] = {new Quaternion(), new Quaternion()};
    private static final Vector3f position[] = {new Vector3f(), new Vector3f()};
    private static VRTextureRenderBuffer screenshotTexture;

    private static volatile boolean takeScreenshot;

    public static void configureHmd(final WorldManager worldManager) {

        SceneWorker.addWorker(new WorkProcessor.WorkCommit() {

            public void commit() {

                OculusSupport.initializeHmd(false);
                if (isUsingHmd() == false) {
                    return;
                }

                Pointer version = OvrLibrary.INSTANCE.ovr_GetVersionString();
                System.out.println(version.getString(0)); 
                
                OculusSupport.configureHmd(GLContext.getCurrentGL());

                createEyeTexture(worldManager, 0);
                createEyeTexture(worldManager, 1);

                worldManager.getRenderManager().setDesiredFrameRate(95);

                System.out.println("Oculus initialization ends");
//                createScreenshotTexture(worldManager, 0);

            }
        });

    }

    private static void createEyeTexture(final WorldManager worldManager, final int eyeIndex) {
        final OvrSizei textureSize = textureSizes[eyeIndex];
        int width = textureSize.w;
        int height = textureSize.h;

        System.out.println("org.jdesktop.wonderland.client.vr.VRSupport.createEyeTexture() "
                + width + " x " + height);

//        width = 3000;
//        height = 900;        
        final VRTextureRenderBuffer rb = new VRTextureRenderBuffer(RenderBuffer.Target.TEXTURE_2D, width, height, 0);

        rb.setIncludeOrtho(false);

        final Node cameraNode = new Node();
        final ViewManager viewManager = ViewManager.getViewManager();
        viewManager.addViewManagerListener(new ViewManager.ViewManagerListener() {
            public void primaryViewCellChanged(ViewCell oldViewCell, final ViewCell newViewCell) {

                rb.setVRData(eyeIndex, newViewCell, cameraNode, vrSupport);
                worldManager.getRenderManager().getBufferController().getCurrentOnscreenBuffer().setRenderUpdater(vrSupport);

            }
        });

        cameraNode.setLocalTranslation(0, 0, 0);
        CameraNode cn = new CameraNode("VR Camera" + eyeIndex, null);

        cameraNode.attachChild(cn);
        final ViewProperties viewProperties = viewManager.getViewProperties();

        CameraComponent cc = worldManager.getRenderManager().createCameraComponent(cameraNode, // The Node of the camera scene graph
                cn, // The Camera
                width, // Viewport width
                height, // Viewport height
                getFOV(), // Field of view
                (float) width / (float) height, // Aspect ratio
                viewProperties.getFrontClip(), // Front clip
                viewProperties.getBackClip(), // Rear clip
                false // Primary?
        );

        rb.setCameraComponent(cc);

        worldManager.getRenderManager()
                .addRenderBuffer(rb);

        eyeRenderBuffer[eyeIndex] = rb;

    }

    public static Quaternion getOrientation(int eye) {
        OvrQuaternionf rot = poses[eye].Orientation;
        orientation[eye].set(rot.x, -rot.y, rot.z, -rot.w);
        return orientation[eye];
    }

    public static Vector3f getPosition(int eye) {
        OvrVector3f pos = poses[eye].Position;
        position[eye].set(-pos.x, pos.y, -pos.z);
        return position[eye];
    }

    private static void createScreenshotTexture(final WorldManager worldManager, final int eyeIndex) {
        int width = 2560;
        int height = 1440;

//        int width = 800;
//        int height = 600;

        final VRTextureRenderBuffer rb = new VRTextureRenderBuffer(RenderBuffer.Target.TEXTURE_2D,
                width, height, 0);

        rb.setIncludeOrtho(false);

        final Node cameraNode = new Node();
        final ViewManager viewManager = ViewManager.getViewManager();

        viewManager.addViewManagerListener(new ViewManager.ViewManagerListener() {
            public void primaryViewCellChanged(ViewCell oldViewCell, final ViewCell newViewCell) {

                rb.setVRData(eyeIndex, newViewCell, cameraNode, vrSupport);
                worldManager.getRenderManager().getBufferController().getCurrentOnscreenBuffer().setRenderUpdater(vrSupport);
                worldManager.getRenderManager().getBufferController().getCurrentOnscreenBuffer().getCanvas().addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyPressed(KeyEvent e) {
                        char keyChar = e.getKeyChar();
                        if (keyChar == 'p') {
                            takeScreenshot = true;
                        }
                    }

                });

            }
        });

        cameraNode.setLocalTranslation(0, 0, 0);
        CameraNode cn = new CameraNode("VR Screenshot Camera" + eyeIndex, null);

        cameraNode.attachChild(cn);
        final ViewProperties viewProperties = viewManager.getViewProperties();

        CameraComponent cc = worldManager.getRenderManager().createCameraComponent(cameraNode, // The Node of the camera scene graph
                cn, // The Camera
                width, // Viewport width
                height, // Viewport height
                viewProperties.getFieldOfView(), // Field of view
                (float) width / (float) height, // Aspect ratio
                viewProperties.getFrontClip(), // Front clip
                viewProperties.getBackClip(), // Rear clip
                false // Primary?
        );

        rb.setCameraComponent(cc);

        worldManager.getRenderManager()
                .addRenderBuffer(rb);

        screenshotTexture = rb;

        rb.setRenderUpdater(new RenderUpdater() {
            public void update(Object obj) {

                SceneWorker.addWorker(new WorkProcessor.WorkCommit() {

                    public void commit() {
                        if (takeScreenshot) {
                            takeScreenshot = false;

                            int textureId = screenshotTexture.getTexture().getTextureId();

                            com.jogamp.opengl.util.texture.Texture texture = new com.jogamp.opengl.util.texture.Texture(textureId,
                                    GL2.GL_TEXTURE_2D, screenshotTexture.getWidth(),
                                    screenshotTexture.getHeight(),
                                    screenshotTexture.getWidth(),
                                    screenshotTexture.getHeight(), false);

                            try {
                                TextureIO.write(texture, new File("C:\\Users\\cramirez.SBP\\Documents\\Temporal\\Oculus\\Textura.png"));
                            } catch (IOException ex) {
                                Logger.getLogger(VRSupport.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (GLException ex) {
                                Logger.getLogger(VRSupport.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    }
                });
            }
        });
    }

    @Override
    public void renderScene(GL2 gl, int eyeIndex) {

//        System.out.println("****************** Render frame: " + getFrameCount());
        if (eyeRenderBuffer == null) {
            return;
        }
        gl.glMatrixMode(GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(-1, 1, -1, 1, -1, 1);
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();

        RenderBuffer rb = eyeRenderBuffer[eyeIndex];
        TextureRenderBuffer trb = (TextureRenderBuffer) rb;
        final Texture texture = trb.getTexture();

        int textureId = texture.getTextureId();

        gl.glColor3f(1, 1, 1.0f);
        gl.glEnable(GL.GL_TEXTURE_2D);
        gl.glBindTexture(GL.GL_TEXTURE_2D, textureId);

        final int SIZE = 1;
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2d(0, 0);
        gl.glVertex2d(-SIZE, -SIZE);
        gl.glTexCoord2d(1, 0);
        gl.glVertex2i(SIZE, -SIZE);

        gl.glTexCoord2d(1, 1);
        gl.glVertex2i(SIZE,
                SIZE);

        gl.glTexCoord2d(0, 1);
        gl.glVertex2d(-SIZE, SIZE);
        gl.glEnd();
        gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
    }

    public void update(Object obj) {

        this.renderFrame(GLContext.getCurrentGL());

    }

}
