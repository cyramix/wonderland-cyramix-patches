/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jdesktop.wonderland.client.vr;

import com.oculusvr.capi.EyeRenderDesc;
import com.oculusvr.capi.FovPort;
import com.oculusvr.capi.Hmd;
import com.oculusvr.capi.HmdDesc;
import com.oculusvr.capi.LayerEyeFov;
import com.oculusvr.capi.MirrorTexture;
import com.oculusvr.capi.MirrorTextureDesc;
import com.oculusvr.capi.OvrLibrary;
import static com.oculusvr.capi.OvrLibrary.ovrProjectionModifier.ovrProjection_ClipRangeOpenGL;
import com.oculusvr.capi.OvrMatrix4f;
import com.oculusvr.capi.OvrRecti;
import com.oculusvr.capi.OvrSizei;
import com.oculusvr.capi.OvrVector3f;
import com.oculusvr.capi.Posef;
import com.oculusvr.capi.TextureSwapChain;
import com.oculusvr.capi.TextureSwapChainDesc;
import com.oculusvr.capi.ViewScaleDesc;
import javax.media.opengl.GL;
import static javax.media.opengl.GL.GL_COLOR_BUFFER_BIT;
import static javax.media.opengl.GL.GL_DEPTH_BUFFER_BIT;
import javax.media.opengl.GL2;
import javax.media.opengl.GLContext;
import static javax.media.opengl.fixedfunc.GLMatrixFunc.*;
import static com.oculusvr.capi.OvrLibrary.*;

/**
 *
 * @author cramirez
 */
public abstract class OculusSupport  {

    public static final int EYE_COUNT = ovrEyeType.ovrEye_Count;
    private static Hmd hmd;

    private static boolean usingHmd;

    private static final OvrVector3f[] eyeOffsets = OvrVector3f.buildPair();
    private static final FovPort[] fovPorts
            = FovPort.buildPair();
    protected static final Posef[] poses
            = Posef.buildPair();

    private static int frameCount = -1;

    private static HmdDesc hmdDesc;
    protected static OvrSizei[] textureSizes = new OvrSizei[EYE_COUNT];
    private static int width;
    private static int height;
    private static TextureSwapChain swapTexture;
    private static MirrorTexture mirrorTexture;
    private static final LayerEyeFov layer = new LayerEyeFov();
    private static final ViewScaleDesc viewScaleDesc = new ViewScaleDesc();
    private static OvrSizei doubleSize;

    private static int frameBufferId;

    public static void initializeHmd(boolean debug) {
        
        System.out.println("Oculus initialization begins");
        try {
            Hmd.initialize();
        } catch (UnsatisfiedLinkError e) {
            System.out.println("Oculu errors");
            return;
        }

        try {
            Thread.sleep(400);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

        Hmd tempHmd;
        tempHmd = Hmd.create();
        hmd = tempHmd;

        hmdDesc = hmd.getDesc();

        width = 1024;
        height = 768;

        for (int eye = 0; eye < EYE_COUNT; ++eye) {
            fovPorts[eye] = hmdDesc.DefaultEyeFov[eye];
            OvrMatrix4f m = Hmd.getPerspectiveProjection(fovPorts[eye], 0.1f, 1000000f, ovrProjection_ClipRangeOpenGL);

            textureSizes[eye] = hmd.getFovTextureSize(eye, fovPorts[eye], 1.0f);
        }

        usingHmd = true;
    }

    public static void destroy() {
        System.out.println("***********************************org.jdesktop.wonderland.client.vr.OculusSupport.destroy()");
        
        
        if( usingHmd == false) {
            return;
        }

        try {
            final GL gl = GLContext.getCurrentGL();
            gl.glDeleteFramebuffers(GL2.GL_FRAMEBUFFER, new int[]{frameBufferId}, 0);
        } catch (Exception e) {
            
        }
        mirrorTexture.destroy();
        swapTexture.destroy();
        hmd.destroy();
        
    }
    /**
     * @return the usingHmd
     */
    public static boolean isUsingHmd() {
        return usingHmd;
    }

    /**
     * @return the hmd
     */
    public static Hmd getHmd() {
        return hmd;
    }

    public static void configureHmd(GL gl) {

        if( usingHmd == false) {
            return;
        }
        doubleSize = new OvrSizei();
        doubleSize.w = textureSizes[0].w + textureSizes[1].w;
        doubleSize.h = textureSizes[0].h;

        TextureSwapChainDesc desc = new TextureSwapChainDesc();
        desc.Format = ovrTextureFormat.OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
        desc.Width = doubleSize.w;
        desc.Height = doubleSize.h;
        desc.Type = ovrTextureType.ovrTexture_2D;
        
        swapTexture = hmd.createSwapTextureChain(desc);
        
        MirrorTextureDesc mirrorTextureDesc = new MirrorTextureDesc();
        mirrorTextureDesc.Format = ovrTextureFormat.OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
        mirrorTextureDesc.Width = doubleSize.w;
        mirrorTextureDesc.Height = doubleSize.h;

        mirrorTexture = hmd.createMirrorTexture(mirrorTextureDesc);

        layer.Header.Type = OvrLibrary.ovrLayerType.ovrLayerType_EyeFov;
        layer.ColorTexure[0] = swapTexture;
        layer.Fov = fovPorts;
        layer.RenderPose = poses;
        for (int eye = 0; eye < EYE_COUNT; ++eye) {
            layer.Viewport[eye].Size = textureSizes[eye];
        }
        layer.Viewport[1].Pos.x = layer.Viewport[1].Size.w;
        layer.Viewport[1].Pos.y = 0;

        int[] tempId = new int[1];
        gl.glGenFramebuffers(GL2.GL_FRAMEBUFFER, tempId, 0);
        frameBufferId = tempId[0];

        for (int eye = 0; eye < ovrEyeType.ovrEye_Count; ++eye) {
            EyeRenderDesc eyeRenderDesc = hmd.getRenderDesc(eye, fovPorts[eye]);
            eyeOffsets[eye].x = eyeRenderDesc.HmdToEyeOffset.x;
            eyeOffsets[eye].y = eyeRenderDesc.HmdToEyeOffset.y;
            eyeOffsets[eye].z = eyeRenderDesc.HmdToEyeOffset.z;
        }
        viewScaleDesc.HmdSpaceToWorldScaleInMeters = 1.0f;
    }

    public static void beginFrame() {
        if( usingHmd == false) {
            return;
        }
        
        ++frameCount;
//        System.out.println("beginFrame: " + frameCount);
        Posef eyePoses[] = hmd.getEyePoses(getFrameCount(), eyeOffsets);
        
        for (int eye = 0; eye < EYE_COUNT; eye++) {
            
            Posef pose = eyePoses[eye];
            // This doesn't work as it breaks the contiguous nature of the array
            // FIXME there has to be a better way to do this
            poses[eye].Orientation = pose.Orientation;
            poses[eye].Position = pose.Position;
        }
    }

    public static void endFrame() {
//        System.out.println("endFrame: " + getFrameCount());
        if( usingHmd == false) {
            return;
        }

        swapTexture.commit();
        hmd.submitFrame(getFrameCount(), layer);
    }

    /**
     * @return the frameCount
     */
    public static int getFrameCount() {
        return frameCount;
    }

    public void renderFrame(GL gl1) {
        if( usingHmd == false) {
            return;
        }
        
        GL2 gl = gl1.getGL2();

        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, frameBufferId);
        final int swapTextId = swapTexture.getTextureId(swapTexture.getCurrentIndex());
        gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_COLOR_ATTACHMENT0,
                GL.GL_TEXTURE_2D, swapTextId, 0);

        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glClearDepth(1.0f);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        for (int eye = 0; eye < EYE_COUNT; ++eye) {

            OvrRecti vp = layer.Viewport[eye];
            gl.glScissor(vp.Pos.x, vp.Pos.y, vp.Size.w, vp.Size.h);
            gl.glViewport(vp.Pos.x, vp.Pos.y, vp.Size.w, vp.Size.h);

            renderScene(gl, eye);
        }

        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);

        endFrame();

//        drawMirrorTexture(gl);
        
    }

    public static float getFOV() {
        if( usingHmd == false) {
            return 0;
        }
        
        return (float)Math.atan(hmdDesc.DefaultEyeFov[0].DownTan) * 2f * 57.2957795f;
    }    
    private static void drawMirrorTexture(GL2 gl) {
        
        gl.glMatrixMode(GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(-1, 1, -1, 1, -1, 1);
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();

        gl.glColor3f(1, 1, 1.0f);
        
        gl.glEnable(GL.GL_TEXTURE_2D);
        
        gl.glBindTexture(GL.GL_TEXTURE_2D, mirrorTexture.getTextureId());

        gl.glScissor(0, 0, width, height);
        gl.glViewport(0, 0, width, height);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        final  int SIZE = 1;
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2d(0, 1);
        gl.glVertex2d(-SIZE, -SIZE);
        gl.glTexCoord2d(1, 1);
        gl.glVertex2i(SIZE, -SIZE);

        gl.glTexCoord2d(1, 0);
        gl.glVertex2i(SIZE,
                SIZE);

        gl.glTexCoord2d(0, 0);
        gl.glVertex2d(-SIZE, SIZE);
        gl.glEnd();
        gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
        
    }

    public abstract void renderScene(GL2 gl, int eyeIndex);

    
}
