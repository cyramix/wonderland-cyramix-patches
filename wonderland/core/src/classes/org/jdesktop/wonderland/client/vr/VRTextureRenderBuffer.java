
package org.jdesktop.wonderland.client.vr;

import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.renderer.Renderer;
import com.jme.scene.Node;
import com.jme.scene.Spatial;
import javolution.util.FastList;
import org.jdesktop.mtgame.PassComponent;
import org.jdesktop.mtgame.TextureRenderBuffer;
import org.jdesktop.mtgame.WorldManager;
import org.jdesktop.wonderland.client.cell.view.ViewCell;
import org.jdesktop.wonderland.client.jme.ClientContextJME;
import static org.jdesktop.wonderland.client.vr.VRSupport.CAMERA_ELEVATION;
import static org.jdesktop.wonderland.client.vr.VRSupport.getOrientation;
import static org.jdesktop.wonderland.client.vr.VRSupport.getPosition;
import org.jdesktop.wonderland.common.cell.CellTransform;

public class VRTextureRenderBuffer extends TextureRenderBuffer {

    private int eyeIndex;
    private ViewCell viewCell;
    private Node cameraNode;
    private VRSupport vrSupport;

    /* Avoid excesive garbage */
    private final Vector3f translation = new Vector3f();
    private final Quaternion rotation = new Quaternion();
    
    public VRTextureRenderBuffer(Target target, int width, int height, int order) {
        super(target, width, height, order);
    }

    public void setVRData(int eyeIndex, final ViewCell viewCell, final Node cameraNode, VRSupport vrSupport) {
        this.eyeIndex = eyeIndex;
        this.viewCell = viewCell;
        this.cameraNode = cameraNode;
        this.vrSupport = vrSupport;
    }
    
    private void updateCameras() {
        if(viewCell == null) {
            return;
        }
        
//        System.out.println("****************** update camera: " + getFrameCount() + " eye: " + eyeIndex);

        CellTransform transform = viewCell.getWorldTransform();
        transform.getTranslation(translation);
        transform.getRotation(rotation);

        Quaternion look = getOrientation(eyeIndex);
        rotation.mult(look, look);

        final WorldManager wm = ClientContextJME.getWorldManager();

        final Vector3f pos = getPosition(eyeIndex);
        rotation.multLocal(pos);
        pos.addLocal(translation);

        pos.addLocal(0, CAMERA_ELEVATION, -0.2f);

        cameraNode.setLocalRotation(look);
        cameraNode.setLocalTranslation(pos);

        wm.addToUpdateList(cameraNode);

    }

    @Override
    public void preparePass(Renderer renderer, FastList<Spatial> rl, FastList<PassComponent> passList, int pass) {
        super.preparePass(renderer, rl, passList, pass); 

        /* Beginning of first texture */
        if( vrSupport != null && eyeIndex == 1) {
            OculusSupport.beginFrame();
            
//            System.out.println("****************** begin frame: " + getFrameCount());
            
        }
        
        updateCameras();
    }

    
}
